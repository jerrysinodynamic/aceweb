package com.concretepage.config;  
  
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.ace.dao.AgentDao;
import com.mchange.v2.c3p0.DriverManagerDataSource;
  
@Configuration 
@ComponentScan({"com.concretepage","com.ace.dao"})
@EnableWebMvc   
public class AppConfig {  
	@Bean
	public DataSource dataSource(){
	    //checkAppEnv("dataSource entry");
	    DriverManagerDataSource ds = new DriverManagerDataSource();
	    
	    ds.setDriverClass("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	    ds.setJdbcUrl("jdbc:sqlserver://10.0.2.213:1433;databaseName=FWDSalesActivity");
	    ds.setUser("sa");
	    ds.setPassword("1teamwork@");
	    //checkAppEnv("dataSource exit");
	    return ds;
	}
	
	@Bean
	public AgentDao agentDao(){
		return new AgentDao();
	}
}  
