package com.concretepage;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ace.dao.AgentDao;
import com.ace.object.Agent;

@Repository
@RestController
@RequestMapping("/data")
public class AgentController {
	
	@Autowired
	private DataSource source;
	
	@Autowired
	private AgentDao agentDao;
	
	@RequestMapping("/agent")
	public Agent getAgentById(@RequestParam(value = "id",required = false,
	                                                    defaultValue = "0") Integer id) {
		try {
			agentDao = new AgentDao();
			return agentDao.getPersonDetail(source.getConnection());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
